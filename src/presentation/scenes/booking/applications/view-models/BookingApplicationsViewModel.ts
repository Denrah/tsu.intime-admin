import BookingUseCase from "../../../../../domain/booking/interactors/BookingUseCase";
import BookingApplication, {BookingApplicationStatus} from "../../../../../domain/booking/models/BookingApplication";

export default class BookingApplicationsViewModel {
    applications: BookingApplication[] = [];

    onDidStartRequest: (() => void) = () => {};
    onDidFinishRequest: (() => void) = () => {};
    onDidUpdate: (() => void) = () => {};
    onDidReceiveError: ((error: Error) => void) = () => {};

    private bookingUseCase: BookingUseCase;

    constructor(bookingUseCase: BookingUseCase) {
        this.bookingUseCase = bookingUseCase;
    }

    loadData() {
        this.onDidStartRequest();
        this.bookingUseCase.getBookingApplications().then(applications => {
            this.applications = applications;
            this.onDidUpdate();
        }).catch(error => {
            this.onDidReceiveError(error);
        }).finally(() => {
            this.onDidFinishRequest();
        });
    }

    approveBooking(bookingID: string) {
        this.onDidStartRequest();
        this.bookingUseCase.approveBooking(bookingID).then(() => {
            this.updateItem(bookingID, BookingApplicationStatus.approved);
            this.onDidUpdate();
        }).catch(error => {
            this.onDidReceiveError(error);
        }).finally(() => {
            this.onDidFinishRequest();
        });
    }

    rejectBooking(bookingID: string) {
        this.onDidStartRequest();
        this.bookingUseCase.rejectBooking(bookingID).then(() => {
            this.updateItem(bookingID, BookingApplicationStatus.rejected);
            this.onDidUpdate();
        }).catch(error => {
            this.onDidReceiveError(error);
        }).finally(() => {
            this.onDidFinishRequest();
        });
    }

    private updateItem(bookingID: string, status: BookingApplicationStatus) {
        let index = this.applications.findIndex(application => application.id === bookingID)
        if(index !== -1) {
            this.applications[index].status = status;
        }
        this.applications.sort((lhs: BookingApplication) => {
            return lhs.status === BookingApplicationStatus.new ? 1 : 0
        })
    }
}