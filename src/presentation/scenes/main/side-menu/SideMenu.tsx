import React from "react";
import {Menu} from "antd";
import {LogoutOutlined, ScheduleOutlined, TeamOutlined} from "@ant-design/icons";
import {Link} from "react-router-dom";
import UserProfile from "../../../../domain/common/models/UserProfile";
import UserRole from "../../../../domain/common/models/UserRole";

interface SideMenuProps {}

interface SideMenuState {
    roles: UserRole[];
}

export default class SideMenu extends React.Component<SideMenuProps, SideMenuState> {
    constructor(props: SideMenuProps) {
        super(props);
        this.state = {
            roles: []
        }
    }
    componentDidMount() {
        let data = window.localStorage.getItem("userProfile");
        if (data) {
            let profile = JSON.parse(data) as UserProfile;
            this.setState({
                roles: profile.roles
            });
        }
    }


    render() {
        return (
            <Menu theme="dark" defaultSelectedKeys={['applications']} mode="inline">
                <Menu.SubMenu
                    key={"booking"}
                    icon={<ScheduleOutlined/>}
                    title={<span>Бронирование</span>}>
                    <Menu.Item key={"applications"}>
                        <Link to="/booking/applications">
                            Заявки
                        </Link>
                    </Menu.Item>
                </Menu.SubMenu>
                <Menu.Item key={"users"} hidden={this.state.roles.indexOf(UserRole.admin) === -1} icon={<TeamOutlined/>}>
                    <Link to="/users">
                        Пользователи
                    </Link>
                </Menu.Item>
                <Menu.Item key={"logout"} icon={<LogoutOutlined/>}>
                    <Link to="/logout">
                        Выход
                    </Link>
                </Menu.Item>
            </Menu>
        );
    }
}