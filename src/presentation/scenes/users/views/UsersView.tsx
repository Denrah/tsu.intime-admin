import React from "react";
import {AutoComplete, Card, Button, Table, Typography, Spin} from "antd";
import {DeleteOutlined} from '@ant-design/icons';
import "./users-view.scss";
import UsersViewModel from "../view-models/UsersViewModel";
import StaffInfo from "../../../../domain/users/models/StaffInfo";
import Notifications from "../../../utils/Notifications";

const {Title} = Typography;

interface UsersViewProps {
    viewModel: UsersViewModel;
}

interface UsersViewState {
    staff: StaffInfo[];
    bookingAdmins: StaffInfo[];
    selectedUser?: string;
    isLoading: boolean;
}

export default class UsersView extends React.Component<UsersViewProps, UsersViewState> {
    private viewModel: UsersViewModel;

    constructor(props: UsersViewProps) {
        super(props);
        this.state = {
            staff: [],
            bookingAdmins: [],
            isLoading: false
        };
        this.viewModel = props.viewModel;
    }

    componentDidMount() {
        this.bindToViewModel();
        this.viewModel.loadData();
    }

    private bindToViewModel() {
        this.viewModel.onDidStartRequest = () => {
            this.setState({
                isLoading: true
            });
        };
        this.viewModel.onDidFinishRequest = () => {
            this.setState({
                isLoading: false
            });
        };
        this.viewModel.onDidUpdate = () => {
            this.setState({
                staff: this.viewModel.staff,
                bookingAdmins: this.viewModel.bookingAdmins
            });
        };
        this.viewModel.onDidReceiveError = (error) => {
            Notifications.handle(error);
        };
    }

    render() {
        return (
            <div className="users-page-container">
                <Title>Модераторы</Title>
                <Card className="users-content-container">
                    {/* @ts-ignore */}
                    <Spin tip="Загрузка..." spinning={this.state.isLoading}>
                        <div className="search-container">
                            <AutoComplete
                                className="search-input"
                                filterOption
                                placeholder="Поиск по ФИО сотрудника"
                                onSelect={(value: string, option: any) => {
                                    console.log(option);
                                    this.setState({
                                        selectedUser: option.key
                                    });
                                }}>
                                {
                                    this.state.staff.map(item => {
                                        return <AutoComplete.Option key={item.accountId} value={item.name}>
                                            {item.name}
                                        </AutoComplete.Option>
                                    })
                                }
                            </AutoComplete>
                            <Button type="primary" onClick={() => {
                                this.viewModel.addBookingAdmin(this.state.selectedUser);
                            }}>
                                Добавить
                            </Button>
                        </div>
                        <Table dataSource={this.state.bookingAdmins} rowKey={record => record.id}>
                            <Table.Column title="ФИО" dataIndex="name" key="name"/>
                            <Table.Column title="Подразделение"
                                          dataIndex="department"
                                          key="department"
                                          render={(text, record: StaffInfo) => record.department.name}/>
                            <Table.Column dataIndex="actions" key="actions" render={(text, record: StaffInfo) => (
                                <Button danger icon={<DeleteOutlined/>} onClick={() => {
                                    this.viewModel.removeBookingAdmin(record.accountId);
                                }}/>
                            )}/>
                        </Table>
                    </Spin>
                </Card>
            </div>
        );
    }
}