import UsersUseCase from "../../../../domain/users/interactors/UsersUseCase";
import StaffInfo from "../../../../domain/users/models/StaffInfo";

export default class UsersViewModel {
    staff: StaffInfo[] = [];
    bookingAdmins: StaffInfo[] = [];

    onDidStartRequest: (() => void) = () => {};
    onDidFinishRequest: (() => void) = () => {};
    onDidUpdate: (() => void) = () => {};
    onDidReceiveError: ((error: Error) => void) = () => {};

    private usersUseCase: UsersUseCase;

    constructor(usersUseCase: UsersUseCase) {
        this.usersUseCase = usersUseCase;
    }

    loadData() {
        this.onDidStartRequest();
        this.usersUseCase.getStaff().then(staff => {
            this.staff = staff;
            this.updateBookingAdmins();
            this.onDidUpdate();
        }).catch(error => {
            this.onDidReceiveError(error);
        }).finally(() => {
            this.onDidFinishRequest();
        });
    }

    addBookingAdmin(accountId?: string) {
        if (accountId) {
            this.onDidStartRequest();
            this.usersUseCase.addBookingAdmin(accountId).then(() => {
                const index = this.staff.findIndex(item => item.accountId == accountId)
                this.staff[index].isBookingAdmin = true;
                this.updateBookingAdmins();
                this.onDidUpdate();
            }).catch(error => {
                this.onDidReceiveError(error);
            }).finally(() => {
                this.onDidFinishRequest();
            });
        }
    }

    removeBookingAdmin(accountId?: string) {
        if (accountId) {
            this.onDidStartRequest();
            this.usersUseCase.removeBookingAdmin(accountId).then(() => {
                const index = this.staff.findIndex(item => item.accountId == accountId)
                this.staff[index].isBookingAdmin = false;
                this.updateBookingAdmins();
                this.onDidUpdate();
            }).catch(error => {
                this.onDidReceiveError(error);
            }).finally(() => {
                this.onDidFinishRequest();
            });
        }
    }

    private updateBookingAdmins() {
        this.bookingAdmins = this.staff.filter(item => item.isBookingAdmin);
    }
}