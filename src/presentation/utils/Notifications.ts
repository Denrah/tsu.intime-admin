import {notification} from "antd";

export default class Notifications {
    static handle(error: Error) {
        notification.error({
            message: error.message
        });
    }
}