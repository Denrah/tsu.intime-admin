import AuthRepository from "../../domain/auth/repository/AuthRepository";
import AuthService from "../../data/network-service/services/AuthService";
import DataStore from "../../data/data-store/DataStore";
import LocalDataStore from "../../data/data-store/LocalDataStore";
import StaffRepository from "../../domain/users/repository/StaffRepository";
import StaffService from "../../data/network-service/services/StaffService";
import BookingRepository from "../../domain/booking/repository/BookingRepository";
import BookingService from "../../data/network-service/services/BookingService";

class DependenciesContainer {
    dataStore: DataStore;
    authRepository: AuthRepository;
    staffRepository: StaffRepository;
    bookingRepository: BookingRepository;

    constructor() {
        this.dataStore = new LocalDataStore();
        this.authRepository = new AuthService(this.dataStore);
        this.staffRepository = new StaffService(this.dataStore);
        this.bookingRepository = new BookingService(this.dataStore);
    }
}

const Dependencies = new DependenciesContainer();

export default Dependencies;