import React from 'react';
import MainRouter from "./presentation/routing/MainRouter";
import 'antd/dist/antd.css';
import {ConfigProvider} from "antd";
import ruRU from 'antd/es/locale/ru_RU';

function App() {
  return (
    <ConfigProvider locale={ruRU}>
      <MainRouter/>
    </ConfigProvider>
  );
}

export default App;
