import NetworkService from "../NetworkService";
import HTTPMethod from "../HTTPMethod";
import BookingRepository from "../../../domain/booking/repository/BookingRepository";
import BookingApplication from "../../../domain/booking/models/BookingApplication";
import EmptyResponse from "../EmptyResponse";

export default class BookingService extends NetworkService implements BookingRepository{
    getBookingApplications(): Promise<BookingApplication[]> {
        return this.request("/bookings", HTTPMethod.get)
    }

    approveBooking(bookingID: string): Promise<EmptyResponse> {
        return this.request(`/bookings/${bookingID}/approve`, HTTPMethod.post);
    }

    rejectBooking(bookingID: string): Promise<EmptyResponse> {
        return this.request(`/bookings/${bookingID}/reject`, HTTPMethod.post);
    }
}