import NetworkService from "../NetworkService";
import HTTPMethod from "../HTTPMethod";
import StaffRepository from "../../../domain/users/repository/StaffRepository";
import StaffInfo from "../../../domain/users/models/StaffInfo";
import EmptyResponse from "../EmptyResponse";

export default class StaffService extends NetworkService implements StaffRepository {
    getStaff(): Promise<StaffInfo[]> {
        return this.request("/staff", HTTPMethod.get);
    }

    addBookingAdmin(accountId: string): Promise<EmptyResponse> {
        return this.request(`/staff/${accountId}/booking-admins`, HTTPMethod.post);
    }

    removeBookingAdmin(accountId: string): Promise<EmptyResponse> {
        return this.request(`/staff/${accountId}/booking-admins`, HTTPMethod.delete);
    }
}