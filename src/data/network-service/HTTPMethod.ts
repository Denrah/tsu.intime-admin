enum HTTPMethod {
    get = "GET",
    post = "POST",
    delete = "DELETE"
}

export default HTTPMethod;