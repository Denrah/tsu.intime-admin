const  NetworkConstants = {
    apiBaseURL: process.env.REACT_APP_API_HOST + "/api/admin/v1"
};

export default NetworkConstants;