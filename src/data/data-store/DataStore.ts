import UserProfile from "../../domain/common/models/UserProfile";

export default interface DataStore {
    accessToken: string | null;
    refreshToken: string | null;
    userProfile: UserProfile | null;
}