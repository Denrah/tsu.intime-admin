import UserProfile from "../../common/models/UserProfile";
import AuthTokenInfo from "./AuthTokenInfo";

export default interface AuthResponse {
    token: AuthTokenInfo;
    profile: UserProfile;
}