export default interface AuthTokenInfo {
    accessToken: string;
    refreshToken: string;
}