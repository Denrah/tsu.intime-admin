import AuthResponse from "../models/AuthResponse";

export default interface AuthRepository {
    loginWithEmail(email: string, password: string): Promise<AuthResponse>
    loginWithTsuAccount(token: string): Promise<AuthResponse>
    logout(): void
}