import StaffRepository from "../repository/StaffRepository";
import StaffInfo from "../models/StaffInfo";
import EmptyResponse from "../../../data/network-service/EmptyResponse";

export default class UsersUseCase {
    private staffRepository: StaffRepository;

    constructor(staffRepository: StaffRepository) {
        this.staffRepository = staffRepository;
    }

    getStaff(): Promise<StaffInfo[]> {
        return this.staffRepository.getStaff();
    }

    addBookingAdmin(accountId: string): Promise<EmptyResponse> {
        return this.staffRepository.addBookingAdmin(accountId);
    }

    removeBookingAdmin(accountId: string): Promise<EmptyResponse> {
        return this.staffRepository.removeBookingAdmin(accountId);
    }
}