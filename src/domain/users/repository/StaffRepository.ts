import StaffInfo from "../models/StaffInfo";
import EmptyResponse from "../../../data/network-service/EmptyResponse";

export default interface StaffRepository {
    getStaff(): Promise<StaffInfo[]>
    addBookingAdmin(accountId: string): Promise<EmptyResponse>
    removeBookingAdmin(accountId: string): Promise<EmptyResponse>
}