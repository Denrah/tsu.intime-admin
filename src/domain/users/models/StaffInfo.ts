interface StaffDepartment {
    id: number;
    name: string;
}

export default interface StaffInfo {
    id: number;
    name: string;
    accountId: string;
    isBookingAdmin: boolean;
    department: StaffDepartment;
}