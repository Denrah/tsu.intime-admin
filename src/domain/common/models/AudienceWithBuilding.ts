import Building from "./Building";

export default interface AudienceWithBuilding {
    id?: string;
    name: string;
    shortName?: string;
    building?: Building;
}