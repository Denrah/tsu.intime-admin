export default interface TsuAccountProfile {
    accountId: string;
    avatarUrl: string;
    lastName: string;
    secondName: string;
    firstName: string;
}