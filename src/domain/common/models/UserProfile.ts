import UserRole from "./UserRole";
import TsuAccountProfile from "./TsuAccountProfile";

export default interface UserProfile {
    id: string;
    email?: string;
    roles: UserRole[];
    tsuAccountProfile: TsuAccountProfile;
}