enum UserRole {
    admin = "ADMIN",
    staff = "STAFF",
    bookingAdmin = "BOOKING_ADMIN"
}

export default UserRole;