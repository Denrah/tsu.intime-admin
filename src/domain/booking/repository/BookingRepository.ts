import BookingApplication from "../models/BookingApplication";
import EmptyResponse from "../../../data/network-service/EmptyResponse";

export default interface BookingRepository {
    getBookingApplications(): Promise<BookingApplication[]>
    approveBooking(bookingID: string): Promise<EmptyResponse>
    rejectBooking(bookingID: string): Promise<EmptyResponse>
}